# For Good Eyes Only Licence
_created by Pixelcode for Free Software_

– Version 0.1 –


## CONTENTS
- [Summary](#summary-1)
- [Preamble](#preamble)
- [Purpose](#purpose)
- [Definitions](#definitions)
- [Interpretation](#interpretation)
- [Scope](#scope)
- [Acceptance](#acceptance)
- [Permission](#permission)
- [Irrevocability](#irrevocability)
- [Change of Licence](#change-of-licence)
- [Licensee's obligations](#licensee-s-obligations)
- [Licensee's rights](#licensee-s-rights)
- [Licensee's prohibitions](#licensee-s-prohibitions)
- [Termination](#termination)
- [Exclusion](#exclusion)
- [Defectiveness](#defectiveness)
- [Licensor's duties](#licensor-s-duties)
- [Violation](#violation)
- [No Liability or Warranty](#no-liability-or-warranty)
- [Indemnity](#indemnity)


## Summary (1)

### The Licensee is allowed to:
- re-distribute, modify and use the original work gratis
- commercialise and patent an extensive product which also contains the adaptation
- use a custom licence for an extensive product which also contains the adaptation

### The Licensee is obliged to:
- credit the original author
- maintain the exact same licence for the adaptation
- document changes to the original
- make the adaptation distinguishable from the original
- always comply with ethical principles
- check the original work for defects before interaction
- preserve the user’s dignity, privacy and data security

### The Licensee is not allowed to:
- commercialise or patent the original or an adaptation as a stand-alone product
- behave in unethical ways

### Other conditions:
- the Licence does not provide any guarantee, warranty or liability
- rights once granted to the Licensee cannot be withdrawn by the Licensor
- the Licence may be amended/withdrawn
- all Licence amendments must be stated transparently
- violations of the terms and conditions result in the loss of the rights granted by the Licence
- the Licensor may make exceptions for logos, icons and the name of the work
- licensed software works have to be offered entirely, free of charge, non-obfuscated and in a common form as far as possible

## Preamble

Conscious of his/her responsibility before God and humankind, impelled by the will to serve the peace of the world, the Licensor, by virtue of his/her authorship rights, has placed his/her work under this For Good Eyes Only Licence.(2)

**The human dignity shall be inviolable.** Respecting it is the obligation of all power of the Licensee and the Licensor.(3)

The Licensee and the Licensor thus profess inviolable and inalienable human rights as the foundation of every human community, of peace and of justice in the world.(4)

## Purpose

This Licence’s purpose is promoting and supporting a sustainable and dignified world by attaching conditions of justice and morality to the use and re-distribution of the Licensed Work.(5)

## Definitions

### Copyright
The term „Copyright“ (or simply „rights“) refers to legal rights a creator of literary, scientific, artistic, software or other works holds to the respective Licensed Work, including patenting rights. These legal rights include at least those granted by the Berne Convention for the Protection of Literary and Artistic Works.

### Licensor
The Licensor is the party that has placed the Licensed Work under this Licence by virtue of its copyright.

### Licensee
The Licensee is any person or entity receiving a copy of the Licensed Work.

### Adaptation
An adaptation is a partial or complete version of the Licensed Work that is used or re-distributed by the Licensee, possibly also in a modified form.

### Extensive product
An extensive product is a collection or combination of at least two individual works that consists predominantly of components other than adaptations of the Licensed Work. The adaptation of the Licensed Work is an addition to the extensive product and is not the main or core function of the extensive work, but at most one of several components.

### Free Democratic Basic Order
The Free Democratic Basic Order is an order which, to the exclusion of any rule of force and arbitrariness, constitutes a rule of law based on the self-determination of the people according to the will of the respective majority and on freedom and equality. The fundamental principles of this order include at least: respect for human rights, especially the right of the personality to life and free development, the sovereignty of the people, the separation of powers, the accountability of the government, the legality of the administration, the independence of the courts, the multi-party principle and equal opportunities for all political parties with the right to form and exercise an opposition in accordance with the constitution.(6)

### Trackers
Trackers are third-party pieces of software whose purpose is to analyse the user’s behaviour regarding the software the tracker is used in. Trackers gather more personal user data than needed to function and send the results back to their creators.

### Extensive contribution to climate change
Extensive contribution to climate change is defined as abetting man-made, environmentally damaging changes in the earth's natural climate on a scale at least 100 times greater than that of persons or other legal entities comparable to the Licensee in relevant respects.

### Commercialisation
Commercialisation is the direct or indirect generation of profit from part or all of the Licensed Work or an adaptation thereof.

### Forests 
Forests are defined as 0.5 or more hectares of trees that were either planted more than 50 years ago or were not planted by humans or human-made equipment.(7)

### Deforestation
Deforestation is defined as the clearing, burning or destruction of 0.5 or more hectares of forests within a 1 year period.(7)

## Interpretation

This Licence shall be interpreted on a common-sense basis and as a coherent, non-absurd and non-contradictory flow of ideas, thoughts and moral points of view. Definitional gaps shall not be interpreted in a manner inconsistent with the intent and the essence of this Licence.

This Licence shall be interpreted in British Standard English.

If any part of this Licence is found to be invalid by a court of law, this shall not affect the remainder of the Licence or the Licensor's copyright.

## Scope

The terms of this Licence can only apply to those works which are copyrightable as well as actually protected, and only where any copyright would be infringed in the hypothetical absence of this Licence. It is subject to the country-of-protection principle of international copyright law.

## Acceptance

This Licence is automatically offered to every person and entity subject to its terms and conditions. The Licensee accepts this Licence and agrees to its terms and conditions by taking any action with the Licensed Work that, absent this Licence, would infringe any intellectual property right held by the Licensor.(5)

### Permission

The Licensor grants by this License to the Licensee, free of charge, permission to do anything with the Licensed Work, to the extent of the Licensor's rights under applicable copyright and patent law, that would otherwise infringe (a) the Licensor's copyright in the Licensed Work or (b) any patent claims in the Licensed Work that the Licensor may license or will license, subject to all of the  terms and conditions of this Licence.(5)
The Licensor has the right to exclude the name of the Licensed Work as well as its logos and icons from this permission. If he/she wishes to do so, he/she is obliged to clearly indicate this condition in this Licence and prominently alongside the Licensed Work.

## Irrevocability

As long as the Licensee fully complies with the terms of this Licence, the Licensor may not revoke any rights once granted to the Licensee under this Licence. This does not include future versions of the Licensed Work, but just that version of the Work licensed under this exact Licence.

## Change of Licence

The Licensor has the right to modify the licence of past, current and future versions of the Licensed Work at any time. However, he/she is obliged to transparently present a chronological sequence of the licences used, including the dates of modification/replacement.

In the event of a licence amendment to past versions or the current version of the Licensed Work, only the new licence adopted by the amendment will apply with respect to those persons or other legal entities who commence use of the Licensed Work subsequent to the effective date of the amendment. They may not invoke the replaced licence.

## Licensee's obligations

The Licensee is obliged 
1. to make his/her adaptations clearly distinguishable from the Licensed Work by
    - (a) modifying its name, logos and icons to an appropriate extent,
    - (b) prominently stating any changes, removals and additions,
2. to protect the digital security and privacy of his/her software adaptation’s users to the best of his/her knowledge and belief and as far as feasible,
3. to give due credit to the author of the Licensed Work by prominently stating the creator’s name or pseudonym alongside the adaptation and by referring to the place from which the Licensed Work has been taken and
4. to put any adaptations of the Licensed Work under this exact Licence too.

## Licensee's rights

The Licensee is allowed 
1. to commercialise his/her adaptation of the Licensed Work if the adaptation is part of an extensive product,
2. to patent an extensive product that contains the Licensed Work and 
3. to place an extensive product, that contains the Licensed Work, under a separate licence.

## Licensee's prohibitions

The Licensee is not allowed 
1. to commercialise or patent his/her adaptations of the Licensed Work in any way if it is not part of an extensive product and
2. to put his/her adaptions of the Licensed Work under a separate licence if the respective adaption is not part of an extensive product.

## Termination

The terms of this Licence – in relation to the Licensee – and the rights granted to the Licensee shall terminate for life if the Licensee is guilty of
1. intentionally or negligently supporting or committing acts that are suitable to unlawfully restrict any other person's **human rights** as set forth in 
    - (a) the Universal Declaration of Human Rights of the United Nations or
    - (b) the Charter of Fundamental Rights of the European Union,
2. intentionally supporting or committing any criminal offence against **international law** as defined in §§ 6–15 of the German Code of Crimes against International Law(8),
3. intentionally or grossly negligently supporting or committing acts that are suitable to impair or abolish the Free Democratic Basic Order of an internationally generally recognised state,
4. intentionally and actively producing or contributing to weapons of mass destruction,
5. intentionally and directly intruding or exploiting any other person’s **privacy** without his/her explicit permission as defined in the General Data Protection Regulation of the European Union,
6. intentionally and directly contributing to deforestation without weighty, justifying reason,
7. intentionally, directly and extensively contributing to climate change without weighty, justifying reason,
8. intentionally using any third-party analytics trackers in any adaptation(9) of the Licensed Work without the user’s explicit permission,
9. intentionally excluding anyone from using the (Licensee’s) adaptation of the Licensed Work just because he/she rejected analytics tracking,
10. intentionally and directly supporting anyone who is guilty of the offences referred to in points 1, 2 or 3 or
11. intentionally and actively transmitting the Licensed Work to anyone who is guilty of the offences referred to in points 1, 2 or 3.

If the Licensee is guilty of at least one of the above offences, he/she is obliged to destroy all his/her copies, whether public or non-public, of the Licensed Work and to immediately stop all interaction with the Licensed Work.

## Exclusion

Excluded from the provisions of this Licence are any persons or other legal entities which have ever been guilty of one more more of the offences referred to in points 1, 2 or 3 of the „Termination“ Section.

## Defectiveness

The Licensee is urged to examine the Licensed Work for any defect prior to any interaction with it. The Licensee's guilelessness shall not be to the Licensor's detriment.

## Licensor's duties

The Licensor is urged to make the Licensed Work, if it is a software work, available in its entirety, free of charge, non-obfuscated and in a commonly used form to the extent possible, applicable and reasonable. If this is not feasible, the reasons for this circumstance shall be prominently displayed alongside the Licensed Work.

## Violation

Any failure of the Licensee to act according to the terms and conditions of this Licence is both a breach of the Licence and an infringement of the intellectual property rights of the Licensor(5) and results in the immediate termination of this Licence between the Licensor and the Licensee, including all rights granted by the Licence.

## No Liability or Warranty

To the full extent allowed by law, the Licensed Work comes „as is“, without any warranty, express or implied, and the Licensor and any other contributor to the Licensed Work shall not be liable to anyone for any damages or other liability arising from, out of, or in connection with the Licensed Work or this Licence, under any kind of legal claim,(5) at least in the absence of gross negligence and intent to the detriment of the Licensee.

## Indemnity

The Licensee shall hold harmless and indemnify the Licensor (and any other contributor to the Licensed Work) against all losses, damages, liabilities, deficiencies, claims, actions, judgments, settlements, interest, awards, penalties, fines, costs, or expenses of whatever kind, including the Licensor’s reasonable attorneys’ fees, arising out of or relating to the Licensee’s use of the Software,(5) at least in the absence of gross negligence and intent to the detriment of the Licensee. 

---

(1) This short summary is not legally binding and does not replace the actual licence text including all terms and conditions.

(2) cf. Preamble GG

(3) cf. Art. 1 (1) GG

(4) cf. Art. 1 (2) GG

(5) cf. The Hippocratic License 2.1, https://firstdonoharm.dev/version/2/1/license

(6) cf. BVerfGE 2, 1 (Ls. 2, 12 f.)

(7) cf. Do No Harm License, https://github.com/raisely/NoHarm/blob/publish/LICENSE.md

(8) **Remark:** *As of July 2021, an English version of the CCAIL is available under https://www.gesetze-im-internet.de/englisch_vstgb (provided by the Federal Ministry of Justice and Consumer Protection).*

(9) This applies only to the adaptation of the Licensed Work itself and does not apply to the rest of an extensive product that contains the adaptation.